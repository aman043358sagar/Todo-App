# Todo-App
This project is demonstration of Room persistence library with MVVM architecture 

insert             |  delete
:-------------------------:|:-------------------------:
<img src="https://raw.githubusercontent.com/aman043358sagar/Todo-App/master/Files/insert.gif" width="246" height="438">  |  <img src="https://raw.githubusercontent.com/aman043358sagar/Todo-App/master/Files/delete.gif" width="246" height="438">


update             |  search
:-------------------------:|:-------------------------:
<img src="https://raw.githubusercontent.com/aman043358sagar/Todo-App/master/Files/update.gif" width="246" height="438">  |  <img src="https://raw.githubusercontent.com/aman043358sagar/Todo-App/master/Files/search.gif" width="246" height="438">

  
## Built using
- [Kotlin 💙](https://kotlinlang.org/) - Official programming language for Android development.
- [Android Architecture Components](https://developer.android.com/topic/libraries/architecture)
  - [LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
  - [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)
- [Jetpack Navigation Component](https://developer.android.com/guide/navigation/) - For Activity/Fragment navigation.
- [MVVM Architecture](https://www.journaldev.com/20292/android-mvvm-design-pattern) - Architecture pattern.
- [Room persistence library](https://developer.android.com/codelabs/android-room-with-a-view-kotlin#0) - For storing data locally
- [Safe-Args](https://developer.android.com/guide/navigation/navigation-pass-data) - For sharing data between fragments
- [Coroutines](https://kotlinlang.org/docs/reference/coroutines-overview.html) - For asynchronous programming.
- [Material Design Components for Android](https://material.io/android) - For Material UI.
